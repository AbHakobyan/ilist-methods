﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IList_ICollection
{
    class Program
    {
        static void Main(string[] args)
        {
            Colection<int> colect = new Colection<int>(10)
            {
                10,
                20,30,40,50,60,70
            };

            int[] array = new int[3];


            foreach (var item in colect)
            {
                Console.WriteLine(item);
            }

            foreach (var item in colect)
            {
                Console.WriteLine(item);
            }


            colect.Insert(2, 231);
            colect.RemoveAt(2);

            foreach (var item in colect)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }

    class Colection<T> : IList<T>
    {
        public Colection<T> Next { get; set; }
        public Colection<T> Second { get; set; }
        public T Value { get; set; }

        public T this[int index]
        {
            get => GetValue(index);
            set
            {
                if (IsReadOnly)
                {
                    SetValue(index, value);
                }
                else
                    throw new Exception("Index not colection");
            }
        }

        public T GetValue(int index)
        {
            int index2 = 0;
            foreach (var item in this)
            {
                if (index2 == index)
                {
                    return item;
                }
                else
                {
                    index2++;
                }
            }
            throw new IndexOutOfRangeException();
        }

        public void SetValue(int index, T value)
        {
            Colection<T> colection = this;
            if (index > 0 && index < Count)
            {
                for (int i = 0; i < Count; i++)
                {
                    if (index == i)
                    {
                        colection.Value = value;
                    }
                    else
                    {
                        colection = colection.Next;
                    }
                }
            }
        }

        public int Count { get; private set; }

        public bool IsReadOnly => true;

        public Colection(T value)
        {
            Value = value;
        }

        public void Add(T item)
        {
            if (Second == null)
            {
                Count++;
                Next = new Colection<T>(item);
                Second = Next;
            }
            else
            {
                Count++;
                Second.Next = new Colection<T>(item);
                Second = Second.Next;
            }
        }

        public void Clear()
        {
            Next = null;
            Second = null;
            Count = 0;
        }

        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = this[arrayIndex];
                arrayIndex++;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new Enumerator(this);
        }

        public struct Enumerator : IEnumerator<T>
        {
            public Colection<T> Root { get; set; }
            public T Current { get; private set; }

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                
            }

            public Enumerator(Colection<T> node)
            {
                Root = node;
                Current = default(T);
            }

            public bool MoveNext()
            {
                if (Root == null)
                {
                    return false;
                }
                Current = Root.Value;
                Root = Root.Next;
                return true;
            }

            public void Reset()
            {
                Current = default(T);
            }
        }

        public int IndexOf(T item)
        {
            int index = 0;
            Colection<T> colection = this;
            for (int i = 0; i < Count; i++)
            {
                if (colection.Value.Equals(item))
                {
                    return index;
                }
                else
                {
                    if (index == Count -1)
                    {
                        break;
                    }
                    Second = Second.Next;
                    index++;
                }
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            Second = this;
            Colection<T> temp;
            for (int i = 0; i < Count; i++)
            {
                if (index == i + 1)
                {
                    temp = Second.Next;
                    Second.Next = new Colection<T>(item);
                    Count++;
                    Second.Next.Next = temp;
                    break;
                }
                else
                {
                    Second = Second.Next;
                }
            }
        }

        public bool Remove(T item)
        {
            if (Contains(item))
            {
                RemoveAt(IndexOf(item));
                return true;
            }
            else
            {
                return false;
            }
        }

        public void RemoveAt(int index)
        {
            Second = this;
            Colection<T> temp;
            for (int i = 0; i < Count; i++)
            {
                if (index == i + 1)
                {
                    temp = Second.Next;
                    Second.Next = temp.Next;
                    Count--;
                    break;
                }
                else
                {
                    Second = Second.Next;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
